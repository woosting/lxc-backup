#!/usr/bin/env bash


# CONFIGURATION

  BACKUP_TARGET=/srv/lxc/BACKUPS
  BACKUP_POSTFIX=$(date +%Y%m%dt%H%M%S)
  SNAPSHOT_RELNOTE=/srv/lxc/backup-relnote


# INITIALISATION
  
  SNAPSHOT_ONLY=0
  LXC_PATH=$(lxc-config lxc.lxcpath)


  # OPTION FLAG INPUT

    function getInput () {
      local OPTIND s a n h option
      while getopts san:h option
      do
        case "${option}"
         in
          s) # Snapshots only
	    SNAPSHOT_ONLY=1;;
          a) # ALL containers: $(lxc-ls) provides the list
	    LXC_CONTAINERS=($(lxc-ls));;
          n) # SPECIFIC container: given argument provides the container
	    LXC_CONTAINERS=(${OPTARG});;
          h) # HELP (explicit)
             printHelp
             exit 0
          ;;
          \?) # HELP (implicit)
             printHelp
             exit 1
          ;;
        esac
      done
    }


  function printHelp () {
      echo -e "LXC-BACKUP - Tar based backup/migration tool for LXC containers (2018, GNU GENERAL PUBLIC LICENSE)\n"
      echo -e "Usage: lxc-backup ([-s] -a|-n <container>)|-h\n"
      echo -e "Options:"
      echo -e "  -s             Snapshot(s) only (no tar backup)"
      echo -e "  -a             Backup all existing containers"
      echo -e "  -n <container> Backup specific container"
      echo -e "  -h             Prints this helptext.\n"
    }


# EXECUTION

  getInput "$@"


  # ERROR CHECKING

    if [ 0 == ${#LXC_CONTAINERS[@]} ]; then
        echo -e "No containers to archive found or specified!\n"
	printHelp
        exit 1;
    fi


  # PROCESSING

    if [ ${SNAPSHOT_ONLY} -ne "1" ]; then

    	# BACKUP

        for LXC_CONTAINER in ${LXC_CONTAINERS[@]}
        do
		    echo -e "---";
		    echo -e "BACKUPPING: ${LXC_CONTAINER}";
		# START CONTAINER (implicitly accepting error if already started - REQUIRES PRE-TEST!)
		    echo -e "Starting container...";
		    lxc-start -n ${LXC_CONTAINER}
		# CLEANSE APT INSTALLATION/UPGRADE RESIDUAL
		    echo -e "Cleansing container...";
		    lxc-attach -n ${LXC_CONTAINER} -- sh -c "apt-get clean" && \
		# STOP CONTAINER
		    echo -e "Stopping container...";
		    lxc-stop -t 5 -n ${LXC_CONTAINER} && \
		# SNAPSHOT CONTAINER
		    echo -e "Snapshotting container...";
		    echo "  - BACKUP (auto-snapshot)" > ${SNAPSHOT_RELNOTE}  && \
		    lxc-snapshot -c ${SNAPSHOT_RELNOTE} -n ${LXC_CONTAINER} && \
		    lxc-snapshot -LC -n ${LXC_CONTAINER};
		# ARCHIVE CONTAINER (keeping numeric owner for migration purposes)
		    echo -e "Tarring container...";
		    tar --numeric-owner -czf ${BACKUP_TARGET}/${LXC_CONTAINER}-${BACKUP_POSTFIX}.tar.gz -C ${LXC_PATH} --exclude=${LXC_CONTAINER}/snaps ${LXC_CONTAINER}  && \
	       # START CONTAINER
    		    echo -e " ";
		    echo -e "Starting container...";
		    lxc-start -n ${LXC_CONTAINER};
		    lxc-info -n ${LXC_CONTAINER};
    		    echo -e " ";
	done
    else

    # SNAPSHOT

	for LXC_CONTAINER in ${LXC_CONTAINERS[@]}
	do
		echo -e "---";
	       	echo -e "SNAPSHOTTING: ${LXC_CONTAINER}";
		# STOP CONTAINER
			echo -e "Stopping container...";
			lxc-stop -t 5 -n ${LXC_CONTAINER};
		# SNAPSHOT CONTAINER
			echo -e "Snapshotting container...";
			echo "  - AUTO (auto-snapshot)" > ${SNAPSHOT_RELNOTE}  && \
			lxc-snapshot -c ${SNAPSHOT_RELNOTE} -n ${LXC_CONTAINER} && \
			lxc-snapshot -LC -n ${LXC_CONTAINER};
		# START CONTAINER
    			echo -e " ";
			echo -e "Starting container...";
			lxc-start -n ${LXC_CONTAINER} && \
			lxc-info -n ${LXC_CONTAINER};
    			echo -e " ";
	done
    fi
  echo -e "${BACKUP_POSTFIX}";


